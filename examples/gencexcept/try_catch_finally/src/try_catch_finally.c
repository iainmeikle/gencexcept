#include <gencexcept/gencexcept.h>

int main(void)
{
    GCE_TRY
    {
        GCE_THROW(GCE_BAD_ALLOC, "Bad allocation in main.");
    }
    GCE_CATCH(GCE_BAD_ALLOC, ex)
    {
        gce_what(stdout, ex);
        fprintf(stdout, "\n");
        gce_where(stdout, ex);
        fprintf(stdout, "\n");
    }
    GCE_FINALLY
    {
        fprintf(stdout, "Finally!\n");
    }
    GCE_END_TRY

    return 0;
}
