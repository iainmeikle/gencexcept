#include "gencexcept.h"

GCE_THREAD_LOCAL struct GCEJmpBufEntry *gce_jbe = 0;
GCE_THREAD_LOCAL int gce_catch = 0;

const struct GCEExceptionType GCE_EXCEPTION = {
    "Exception.", NULL };

const struct GCEExceptionType GCE_LOGIC_ERROR = {
    "Logic error.", &GCE_EXCEPTION };

const struct GCEExceptionType GCE_INVALID_ARGUMENT = {
    "Invalid argument.", &GCE_LOGIC_ERROR };

const struct GCEExceptionType GCE_DOMAIN_ERROR = {
    "Domain error.", &GCE_LOGIC_ERROR };

const struct GCEExceptionType GCE_LENGTH_ERROR = {
    "Length error.", &GCE_LOGIC_ERROR };

const struct GCEExceptionType GCE_OUT_OF_RANGE = {
    "Out of range.", &GCE_LOGIC_ERROR };

const struct GCEExceptionType GCE_RUNTIME_ERROR = {
    "Runtme error.", &GCE_EXCEPTION };

const struct GCEExceptionType GCE_RANGE_ERROR = {
    "Range error.", &GCE_RUNTIME_ERROR };

const struct GCEExceptionType GCE_OVERFLOW_ERROR = {
    "Overflow error.", &GCE_RUNTIME_ERROR };

const struct GCEExceptionType GCE_UNDERFLOW_ERROR = {
    "Underflow error.", &GCE_RUNTIME_ERROR };

const struct GCEExceptionType GCE_BAD_ALLOC = {
    "Bad alloc.", &GCE_EXCEPTION };

const struct GCEExceptionType GCE_BAD_EXCEPTION = {
    "Bad exception.", &GCE_EXCEPTION };

const int GCE_EXCEPTION_IS_NOT_OF_GIVEN_TYPE = 0;
const int GCE_EXCEPTION_IS_OF_GIVEN_TYPE = 1;
const int GCE_EXCEPTION_IS_A_SUBTYPE_OF_GIVEN_TYPE = 2;

int gce_exception_is_of_type(const struct GCEExceptionType *const type,
    const struct GCEException *const ex)
{
    assert(type);
    assert(ex);

    int result = GCE_EXCEPTION_IS_NOT_OF_GIVEN_TYPE;
    const struct GCEExceptionType *extype = ex->type;

    for (; type != extype; extype = extype->parent)
    {
    }

    if (extype)
    {
        if (ex->type == extype)
        {
            result = GCE_EXCEPTION_IS_OF_GIVEN_TYPE;
        }
        else if (type == extype)
        {
            result = GCE_EXCEPTION_IS_A_SUBTYPE_OF_GIVEN_TYPE;
        }
    }

    return result;
}

void gce_what(FILE *const stream, const struct GCEException *const ex)
{
    assert(stream);
    assert(ex);
    assert(ex->type);
    assert(ex->type->description);

    fprintf(stream, "%s %s", ex->type->description, ex->message);
}

void gce_where(FILE *const stream, const struct GCEException *const ex)
{
    assert(stream);
    assert(ex);
    assert(ex->file);

    fprintf(stream, "%s %u", ex->file, ex->line);
}
