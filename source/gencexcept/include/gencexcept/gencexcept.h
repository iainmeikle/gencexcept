#ifndef C58AD229_4C8B_4C75_859C_EAB981621E11
#define C58AD229_4C8B_4C75_859C_EAB981621E11

#include <assert.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdio.h>
#include <string.h>

/* Microsoft */
#ifdef _MSC_VER
#define GCE_THREAD_LOCAL __declspec(thread)
#endif

/* Digital Mars */
#ifdef __DMC__
#define GCE_THREAD_LOCAL __declspec(thread)
#endif

/* Intel */
#ifdef __ICL
#ifdef _WIN32
#define GCE_THREAD_LOCAL __declspec(thread)
#endif
#ifdef _WIN64
#define GCE_THREAD_LOCAL __declspec(thread)
#endif
#ifdef __gnu_linux__
#define GCE_THREAD_LOCAL __thread
#endif
#endif

/* GCC */
#ifdef __GNUC__
#define GCE_THREAD_LOCAL __thread
#endif

/* Clang */
#ifdef __clang__
#define GCE_THREAD_LOCAL __thread
#endif

/* IBM */
#ifdef __IBMC__
#define GCE_THREAD_LOCAL __thread
#endif

/*
 * @brief A structure to hold information about a type of exception.
 *
 * @description This structure holds a description for a type of exception
 * and the parent (in-order to allow sub-types). A grandparent would be
 * recorded as the parent of the parent (obviously).
 */
struct GCEExceptionType
{
    const char *const description;
    const struct GCEExceptionType *const parent;
};

/*
 * @brief A structure to hold infomation about a thrown exception.
 *
 * @description This structure holds information for a thrown exception,
 * including the type of exception, the file from which the exception was
 * thrown, the line within the throwing file and the point at which
 * the exception was thrown.
 */
struct GCEException
{
    const struct GCEExceptionType *type;
    const char *message;
    const char *file;
    unsigned int line;
};

/*
 * @brief A structure to hold information for entry in the exception stack.
 */
struct GCEJmpBufEntry
{
    jmp_buf jb;
    struct GCEException ex;
    struct GCEJmpBufEntry *outer;
};

/*
 * @brief
 *
 * @description
 */
extern GCE_THREAD_LOCAL struct GCEJmpBufEntry *gce_jbe;

/*
 * @brief
 *
 * @description
 */
extern GCE_THREAD_LOCAL int gce_catch;

/*
 * @brief An exception type to represent a general exception.
 *
 * @description This represents a general exception of any type. All exceptions
 * should be a sub-type of this exception type.
 */
const struct GCEExceptionType GCE_EXCEPTION;

/*
 * @brief An exception type to represent a logic error.
 *
 * @description A logic error could be an invalid argument, domain error,
 * length error, or an out of range error. This is a sub-type of @ref
 * GCE_EXCEPTION exception.
 */
const struct GCEExceptionType GCE_LOGIC_ERROR;

/*
 * @brief An exception type to represent an invalid argument.
 *
 * @description This is a sub-type type of @ref GCE_LOGIC_ERROR exception.
 */
const struct GCEExceptionType GCE_INVALID_ARGUMENT;

/*
 * @brief An exception type to represent a domain error.
 *
 * @description This is a sub-type of @ref GCE_LOGIC_ERROR exception.
 */
const struct GCEExceptionType GCE_DOMAIN_ERROR;

/*
 * @brief An exception type to represent a length error.
 *
 * @description This is a sub-type of @ref GCE_LOGIC_ERROR exception.
 */
const struct GCEExceptionType GCE_LENGTH_ERROR;

/*
 * @brief An exception type to represent an out of range error.
 *
 * @description This is a sub-type of @ref GCE_LOGIC_ERROR exception.
 */
const struct GCEExceptionType GCE_OUT_OF_RANGE;

/*
 * @brief An exception type to represent a runtime error.
 *
 * @description A runtime error could be a range, underflow or overflow
 * error. This is a sub-type of @ref GCE_EXCEPTION exception.
 */
const struct GCEExceptionType GCE_RUNTIME_ERROR;

/*
 * @brief An exception type to represent a range error.
 *
 * @description This is a sub-type of @ref GCE_RUNTIME_ERROR exception.
 */
const struct GCEExceptionType GCE_RANGE_ERROR;

/*
 * @brief An exception type to represent an overflow error.
 *
 * @description This is a sub-type of @ref GCE_RUNTIME_ERROR exception.
 */
const struct GCEExceptionType GCE_OVERFLOW_ERROR;

/*
 * @brief An exception type to represent an underflow error.
 *
 * @description This is a sub-type of @ref GCE_RUNTIME_ERROR exception.
 */
const struct GCEExceptionType GCE_UNDERFLOW_ERROR;

/*
 * @brief An exception type to represent a bad allocation.
 *
 * @description This is a sub-type of @ref GCE_EXCEPTION exception.
 */
const struct GCEExceptionType GCE_BAD_ALLOC;

/*
 * @brief An exception type to represent a bad exception.
 *
 * @description This is a sub-type of @ref GCE_EXCEPTION exception.
 */
const struct GCEExceptionType GCE_BAD_EXCEPTION;

/*
 * @brief A constant to indicate that a @ref GCEException is not of a given
 * @ref GCEExceptionType.
 */
const int GCE_EXCEPTION_IS_NOT_OF_GIVEN_TYPE;

/*
 * @brief A constant to indicate that a @ref GCEException is of a given
 * @ref GCEExceptionType.
 */
const int GCE_EXCEPTION_IS_OF_GIVEN_TYPE;

/*
 * @brief A constant to indicate that a @ref GCEException is a sub-type
 * of a given @ref GCEExceptionType.
 */
const int GCE_EXCEPTION_IS_A_SUBTYPE_OF_GIVEN_TYPE;

/*
 * @brief Determines if the given exception is of the given exception type.
 *
 * @param type The type the given exception is to be compared against.
 * @param ex The exception to be compared against the given type.
 *
 * @return @ref GCE_EXCEPTION_IS_OF_GIVEN_TYPE if the exception is of the
 * given type, @ref GCE_EXCEPTION_IS_A_SUBTYPE_OF_GIVEN_TYPE if the
 * exception is a sub-type of the given type,
 * @ref GCE_EXCEPTION_IS_NOT_OF_GIVEN_TYPE otherwise.
 */
int gce_exception_is_of_type(const struct GCEExceptionType *const type,
    const struct GCEException *const ex);

/*
 * @brief Prints out what the given exception is to the given stream.
 *
 * @param stream The stream to write the exception information to.
 * @param ex The exception to be written to the given stream.
 */
void gce_what(FILE *const stream, const struct GCEException *const ex);

/*
 * @brief Prints out where the given exception occurred to the given stream.
 *
 * @param stream The stream to write the exception location to.
 * @param ex The exception to be written to the given stream.
 */
void gce_where(FILE *const stream, const struct GCEException *const ex);

/*
 * @brief A macro to begin a try block.
 *
 * @description The macro sets up th necessary constructs for try block.
 * A @ref GCE_CATCH block MUST follow.
 */
#define GCE_TRY \
    { \
        struct GCEJmpBufEntry jbe; \
        memset(&jbe, 0, sizeof(jbe)); \
        if (!setjmp(jbe.jb)) \
        { \
            if (gce_jbe) \
                jbe.outer = gce_jbe; \
            gce_jbe = &jbe;

/*
 * @brief A macro to begin a catch block for exceptions of a specified type.
 *
 * @param extype The type of exceptions for which this catch block applies.
 * @param exvar The variable name in which to store the caught exception.
 */
#define GCE_CATCH(extype, exvar) \
        } \
        else if (gce_exception_is_of_type(&(extype), &(jbe.ex))) \
        { \
            const struct GCEException *const (exvar) = &(jbe.ex); \
            gce_catch = 1; \
            (exvar);

/*
 * @brief A maextypecro to begin a finally block.
 *
 * @descriptextypeion A finally block will always be executed.
 */
#define GCE_FINALLY \
        } \
        {

/*
 * @brief
 *
 * @description
 */
#define GCE_END_TRY \
        } \
        if (gce_jbe) \
        { \
            struct GCEJmpBufEntry *jbetop = gce_jbe; \
            gce_jbe = gce_jbe->outer; \
            longjmp(jbetop->jb, 1); \
        } \
    }

/*
 * @brief A macro to count the number of arguments given.
 *
 * @param The arguments to be counted.
 */
#define GCE_ARG_COUNT(...) \
         GCE_ARG_COUNT_F_IMPL(__VA_ARGS__, GCE_ARG_COUNT_R_IMPL())

/*
 * @brief An implementation macro for counting the number of
 * arguments. DO NOT call directly.
 */
#define GCE_ARG_COUNT_F_IMPL(...) \
         GCE_ARG_COUNT_IMPL(__VA_ARGS__)

/*
 * @brief An implementation macro for counting the number of
 * arguments. DO NOT call directly.
 */
#define GCE_ARG_COUNT_IMPL( \
          _1, _2, _3, N, ...) N

/*
 * @brief An implementation macro for counting the number of
 * arguments. DO NOT call directly.
 */
#define GCE_ARG_COUNT_R_IMPL() \
         3, 2, 1, 0

/*
 * @brief A utility macro for invoking a specific macro based
 * on the number of arguments given.
 *
 * @param f The name of the function macro.
 * @param The arguments to be passed to the function macro.
 */
#define GCE_MACRO_DISPATCH(f, ...) \
    GCE_MACRO_DISPATCH_IMPL_1(f, GCE_ARG_COUNT(__VA_ARGS__))

/*
 * @brief An implementation macro for invoking a specific macro
 * based on the number of arguments given. DO NOT call directly.
 *
 * @param f The name of the function macro.
 * @param n The number of the function macro.
 */
#define GCE_MACRO_DISPATCH_IMPL_1(f, n) \
    GCE_MACRO_DISPATCH_IMPL_2(f, n)

/*
 * @brief An implementation macro for invoking a specific macro
 * based on the number of arguments given. DO NOT call directly.
 *
 * @param f The name of the function macro.
 * @param n The number of the function macro.
 */
#define GCE_MACRO_DISPATCH_IMPL_2(f, n) \
    f ## n

/*
 * @brief A macro to throw an exception of the specified type with the
 * specified message.
 *
 * @param The exception or exception type to be (re)thrown.
 */
#define GCE_THROW(...) \
    GCE_MACRO_DISPATCH(GCE_THROW_IMPL_, __VA_ARGS__)(__VA_ARGS__)

/*
 * @brief An implementation macro to throw a previously caught
 * exception. DO NOT call directly.
 *
 * @param exc The exception to be rethrown.
 */
#define GCE_THROW_IMPL_1(exc) \
    do \
    { \
        if (!gce_jbe) \
        { \
            abort(); \
        } \
        gce_jbe->ex = *(ex); \
        if (!gce_catch) \
        { \
            struct GCEJmpBufEntry *jbetop = gce_jbe; \
            gce_jbe = gce_jbe->outer; \
            longjmp(jbetop->jb, 1); \
        } \
        else \
        { \
            gce_catch = 0; \
        } \
    } while (0)

/*
 * @brief A implementation macro to throw an exception of the
 * specified type with the specified message.
 *
 * @param extype The exception type to be thrown.
 * @param exmsg The exception message.
 */
#define GCE_THROW_IMPL_2(extype, exmsg) \
    do \
    { \
        if (!gce_jbe) \
        { \
            abort(); \
        } \
        gce_jbe->ex.type = &(extype); \
        gce_jbe->ex.message = exmsg; \
        gce_jbe->ex.file = __FILE__; \
        gce_jbe->ex.line = __LINE__; \
        if (!gce_catch) \
        { \
            struct GCEJmpBufEntry *jbetop = gce_jbe; \
            gce_jbe = gce_jbe->outer; \
            longjmp(jbetop->jb, 1); \
        } \
        else \
        { \
            gce_catch = 0; \
        } \
    } while (0)

#endif /* C58AD229_4C8B_4C75_859C_EAB981621E11 */
