find_path(GENCEXCEPT_INCLUDE_DIR gencexcept.h
	PATH_SUFFIXES gencexcept/include)
find_library(GENCEXCEPT_LIBRARY 
	NAMES gencexcept gencexceptmsr gencexceptrwdi gencexceptd
	PATH_SUFFIXES gencexcept/lib)

set(GENCEXCEPT_LIBRARIES ${GENCEXCEPT_LIBRARY})
set(GENCEXCEPT_INCLUDE_DIRS ${GENCEXCEPT_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(gencexcept DEFAULT_MSG 
	GENCEXCEPT_LIBRARY GENCEXCEPT_INCLUDE_DIR)

mark_as_advanced(GENCEXCEPT_INCLUDE_DIR GENCEXCEPT_LIBRARY)